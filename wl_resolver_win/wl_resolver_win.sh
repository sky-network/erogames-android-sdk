#!/bin/bash
#
# version: 1.0.2
#

readonly COLOR_RED=$(tput setaf 1)
readonly COLOR_GREEN=$(tput setaf 2)
readonly STYLE_RESET=$(tput sgr0)
TMP_DIR=$(mktemp -d "${TMPDIR:-/tmp}"/sky_win_wl.XXXXXXXX)
declare -r TMP_DIR

# Init named arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
  --whitelabel)
    declare -r WHITELABEL="$2"
    shift
    ;;
  --cert)
    declare -r CERT="$2"
    shift
    ;;
  --pass)
    declare -r PASS="$2"
    shift
    ;;
  --cafile)
    declare -r CA_FILE="$2"
    shift
    ;;
  --tsacafile)
    declare -r TSA_CA_FILE="$2"
    shift
    ;;
  --in)
    declare -r INPUT="$2"
    shift
    ;;
  --out)
    declare OUTPUT="$2"
    shift
    ;;
  *) ;;
  esac
  shift
done

if [[ "$OUTPUT" == "./"* ]]; then
  new_out="${OUTPUT#"./"}"
  OUTPUT="$(pwd)/$new_out"
fi

#######################################
# Сolored 'echo'
# Example 1:
#   echolog "some_string"
#   out: {prefix} some_string
# Example 2:
#   echolog "some_string" ""
#   out: some_string
# Example 3:
#   echolog "some_string" "$COLOR_RED"
#   out: {RED prefix} some_string
# Globals:
#   COLOR_RED, COLOR_GREEN, STYLE_RESET
# Arguments:
#    String to output.
#    (Option) Prefix color. Prefix will not be printed if this arg is empty.
#######################################
function echolog() {
  if [[ "$#" -gt 1 ]]; then
    if [ -z "${2}" ]; then
      prefix=""
      prefix_color="$STYLE_RESET"
    else
      prefix="==>"
      prefix_color="${2}"
    fi
  else
    prefix="==>"
    prefix_color="$COLOR_GREEN"
  fi
  echo "${prefix_color}${prefix}${STYLE_RESET} ${1}"
}

#######################################
# Cleanup dir if exists.
# Arguments:
#    Dir path to cleanup.
#######################################
function remove_dir() {
  local dir_to_remove="${1}"
  if [ -d "$dir_to_remove" ]; then
    echolog "Remove dir: $dir_to_remove" "$COLOR_GREEN"
    rm -rf "$dir_to_remove"
  fi
}

#######################################
# Remove file if exists.
# Arguments:
#    File path to remove.
#######################################
function remove_file() {
  local file_remove="${1}"
  if [ -e "$file_remove" ]; then
    echolog "Remove: $file_remove" "$COLOR_GREEN"
    rm -f "$OUTPUT"
  fi
}

function cleanup_and_exit() {
  remove_dir "$TMP_DIR"
  remove_file "$OUTPUT"
  exit
}

function signature_verify() {
  local signed_file="${1}"
  local verify_params=(-in "$signed_file")

  if [ -n "$CA_FILE" ]; then
    verify_params+=(-CAfile "$CA_FILE")
  fi

  if [ -n "$TSA_CA_FILE" ]; then
    verify_params+=(-TSA-CAfile "$TSA_CA_FILE")
  fi

  osslsigncode verify "${verify_params[@]}"
}

function sign() {
  local in_file="${1}"
  local out_file="${2}"
  osslsigncode sign \
    -pkcs12 "$CERT" \
    -pass "$PASS" \
    -h sha256 \
    -t http://timestamp.digicert.com \
    -in "$in_file" \
    -out "$out_file"
}

if [ -z "$WHITELABEL" ]; then
  echolog "Please specify the '--whitelabel'." "$COLOR_RED"
  exit 1
fi

if [ -z "$CERT" ]; then
  echolog "Please specify the '--cert'." "$COLOR_RED"
  exit 1
fi

if [ -z "$PASS" ]; then
  echolog "Please specify the '--pass'." "$COLOR_RED"
  exit 1
fi

if [ -z "$INPUT" ]; then
  echolog "Please specify the '--in'." "$COLOR_RED"
  exit 1
fi

if [ ! -f "$INPUT" ]; then
  echolog "File not found: $INPUT" "$COLOR_RED"
  exit 1
fi

if [ -z "$OUTPUT" ]; then
  echolog "Please specify the '--out'." "$COLOR_RED"
  exit 1
fi

##############################
# Cleanup temp dir.
if [ -d "$TMP_DIR" ]; then
  echolog "Clean up temp dir..." "$COLOR_GREEN"
  remove_dir "$TMP_DIR"
fi

# Unzip archive into the temp dir.
echolog "Unzip archive..." "$COLOR_GREEN"
unzip -q -d "$TMP_DIR" "$INPUT" || cleanup_and_exit
echolog "Unzipped to: $TMP_DIR" "$COLOR_GREEN"

# Sign exe and dll files
readonly sign_suffix=".wl_signed"
find "$TMP_DIR" -maxdepth 1 -type f -name "*.exe" | while read -r file; do
  echolog "Sign $file" "$COLOR_GREEN"
  rm "${file}${sign_suffix}"

  sign "$file" "${file}${sign_suffix}" || cleanup_and_exit
  signature_verify "${file}${sign_suffix}" || cleanup_and_exit

  rm "$file"
  mv "${file}${sign_suffix}" "$file"
done

find "$TMP_DIR" -maxdepth 1 -type f -name "*.dll" | while read -r file; do
  echolog "Sign $file" "$COLOR_GREEN"
  rm "${file}${sign_suffix}"

  sign "$file" "${file}${sign_suffix}" || cleanup_and_exit
  signature_verify "${file}${sign_suffix}" || cleanup_and_exit

  rm "$file"
  mv "${file}${sign_suffix}" "$file"
done

# Add erogames_whitelabel_id file into "*_Data" folder.
suffix="_Data"
matching_dir=$(find "$TMP_DIR" -type d -name "*$suffix" | head -n 1)
if [ -n "$matching_dir" ]; then
  echolog "Set whitelabel: $WHITELABEL" "$COLOR_GREEN"
  : >"$matching_dir/erogames_whitelabel_id"
  printf %s "$WHITELABEL" >>"$matching_dir/erogames_whitelabel_id"
else
  echolog "No matching directory: *$suffix" "$COLOR_RED"
fi

# Zip
cd "$TMP_DIR" || exit
echolog "Create zip archive..." "$COLOR_GREEN"
remove_file "$OUTPUT"
zip -q -r "$OUTPUT" . || cleanup_and_exit
echolog "Zip archive created." "$COLOR_GREEN"
cd ..

# Remove temp dir
remove_dir "$TMP_DIR"
echolog "Done." "$COLOR_GREEN"
exit
