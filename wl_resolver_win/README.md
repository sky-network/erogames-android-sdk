# Whitelabel Resolver Script (Windows)

...

### Requirements
* [osslsigncode](https://github.com/mtrojnar/osslsigncode)

### How to use
```
bash wl_resolver_win.sh --whitelabel erogames \
  --cert "<path_to.pfx>" \
  --pass "<password>" \
  --cafile <path_to_ca-certificates.crt> \
  --tsacafile <path_to_ca-certificates.crt> \
  --in "<path_to_unity_windows_game.zip>" \
  --out "<path_to_unity_windows_game_out.zip>"
```
