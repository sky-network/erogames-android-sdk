package com.erogames.analytics.model

import android.content.ContentValues
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Keep
@Entity
@Serializable
data class TrackInfo(
    @PrimaryKey
    val id: String,
    val authority: String,
    val createdAt: Long,
) {

    companion object {
        const val ID = "id"
        const val AUTHORITY = "authority"
        const val CREATED_AT = "createdAt"

        fun toContentValues(trackInfo: TrackInfo): ContentValues = ContentValues().apply {
            this.put(ID, trackInfo.id)
            this.put(AUTHORITY, trackInfo.authority)
            this.put(CREATED_AT, trackInfo.createdAt)
        }

        fun fromContentValues(values: ContentValues): TrackInfo = TrackInfo(
            values.getAsString(ID),
            values.getAsString(AUTHORITY),
            values.getAsLong(CREATED_AT))
    }
}