package com.erogames.analytics.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class EventResp(val status: String) {
    fun isSuccessful(): Boolean = "success".equals(status, ignoreCase = true)
}