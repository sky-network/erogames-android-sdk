package com.erogames.analytics.repository

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.erogames.analytics.model.WhitelabelCache
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

internal class Storage private constructor(private val prefs: SharedPreferences) {

    fun setClientId(clientId: String) = prefs.edit().putString(PREF_CLIENT_ID, clientId).apply()

    fun getClientId(): String = prefs.getString(PREF_CLIENT_ID, "")!!

    fun getWhitelabelCache(): WhitelabelCache? {
        val wlCacheStr = prefs.getString(PREF_WL_CACHE, null) ?: return null
        return Json { ignoreUnknownKeys = true }.decodeFromString<WhitelabelCache>(wlCacheStr)
    }

    fun setWhitelabelCache(wlCache: WhitelabelCache) {
        val wlCacheStr = Json { ignoreUnknownKeys = true }.encodeToString(wlCache)
        prefs.edit().putString(PREF_WL_CACHE, wlCacheStr).apply()
    }

    companion object {

        @Volatile
        private var instance: Storage? = null
        private const val PREF_CLIENT_ID = "com_erogames_analytics_pref_client_id"
        private const val PREF_WL_CACHE = "com_erogames_analytics_pref_wl_cache"

        @JvmStatic
        fun getInstance(context: Context): Storage {
            return instance ?: synchronized(this) {
                instance ?: Storage(
                    PreferenceManager.getDefaultSharedPreferences(context)
                ).also { instance = it }
            }
        }
    }
}