package com.erogames.analytics.repository

import android.content.*
import android.database.Cursor
import android.net.Uri
import com.erogames.analytics.db.TrackInfoDao
import com.erogames.analytics.model.TrackInfo
import com.erogames.analytics.util.InjectorUtil

class EATrackInfoProvider : ContentProvider() {

    private lateinit var trackInfoDao: TrackInfoDao

    override fun onCreate(): Boolean {
        trackInfoDao = InjectorUtil.provideDb(context!!).trackInfoDao()
        val authority = getAuthority(context!!)
        URI_MATCHER.addURI(authority, TABLE_NAME, DATA)
        URI_MATCHER.addURI(authority, "$TABLE_NAME/*", DATA_ITEM)
        return true
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?,
    ): Cursor? {
        val cursor: Cursor
        when (URI_MATCHER.match(uri)) {
            DATA -> {
//                cursor = InjectorUtil.provideDb(context!!)
//                    .query("SELECT * FROM $TABLE_NAME WHERE $selection", selectionArgs)
                cursor = trackInfoDao.fetchAllWithCursor()
                if (context != null) {
                    cursor.setNotificationUri(context!!.contentResolver, uri)
                    return cursor
                }
                return null
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun getType(uri: Uri): String? = null

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        when (URI_MATCHER.match(uri)) {
            DATA -> {
                if (context != null) {
                    val id: Long = trackInfoDao.insert(TrackInfo.fromContentValues(values!!))
                    if (id != 0L) {
                        context!!.contentResolver.notifyChange(uri, null)
                        return ContentUris.withAppendedId(uri, id)
                    }
                }
                return null
            }
            DATA_ITEM -> throw IllegalArgumentException("Insert failed: $uri")
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?,
    ): Int {
        when (URI_MATCHER.match(uri)) {
            DATA -> {
                if (context != null) {
                    val count: Int = trackInfoDao.update(TrackInfo.fromContentValues(values!!))
                    if (count != 0) {
                        context!!.contentResolver.notifyChange(uri, null)
                        return count
                    }
                }
                throw IllegalArgumentException("Invalid URI: can't update")
            }
            DATA_ITEM -> throw IllegalArgumentException("Invalid URI: can't update")
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        when (URI_MATCHER.match(uri)) {
            DATA -> throw IllegalArgumentException("Invalid uri: can't delete")
            DATA_ITEM -> {
                if (context != null) {
                    val count: Int = trackInfoDao.delete(ContentUris.parseId(uri))
                    context!!.contentResolver
                        .notifyChange(uri, null)
                    return count
                }
                throw IllegalArgumentException("Unknown URI: $uri")
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    companion object {
        const val TABLE_NAME = "trackinfo"
        private const val AUTHORITY_SUFFIX = ".eaTrackInfoProvider"
        private const val DATA = 1
        private const val DATA_ITEM = 2

        private val URI_MATCHER: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        fun getAuthority(ctx: Context, packageName: String? = null): String {
            if (packageName == null) {
                return "${ctx.packageName}$AUTHORITY_SUFFIX"
            }
            return "$packageName$AUTHORITY_SUFFIX"
        }
    }
}