package com.erogames.analytics

import android.content.Context
import android.text.TextUtils
import android.util.Log
import androidx.annotation.Keep
import com.erogames.analytics.model.EventInfo
import com.erogames.analytics.model.TrackingDataModel
import com.erogames.analytics.util.InjectorUtil
import com.erogames.analytics.util.TrackProviderUtil
import com.erogames.analytics.util.Utils
import com.erogames.analytics.work.EventWorker
import java.util.*

@Keep
class ErogamesAnalytics private constructor() {

    companion object {

        private const val TAG: String = "ErogamesAnalytics"
        private val RESERVED_PREFIXES: List<String> = listOf("ea_")

        /**
         * ErogamesAnalytics initialization.
         *
         * @param ctx [Context]
         * @param clientId
         */
        @JvmStatic
        fun init(ctx: Context, clientId: String) {
            Log.i(TAG, "v${BuildConfig.VERSION_NAME}")
            InjectorUtil.provideRepository(ctx).setClientId(clientId.trim())
        }

        /**
         * Send an event to the server asynchronously.
         * The event will be added to the queue immediately.
         * But sending may be delayed if, for example, there is no internet connection, etc.
         *
         * @param ctx
         * @param event
         * @param params
         */
        @JvmStatic
        @JvmOverloads
        fun logEvent(ctx: Context, event: String, params: Map<String, String> = mapOf()) {
            val nonReservedParams = extractNonReservedParams(params)

            val id = UUID.randomUUID().toString()
            val trackingDataModel = TrackingDataModel(
                event,
                getCategory(params),
                getClientId(ctx),
                getTrackId(ctx),
                getSourceId(ctx, params),
                nonReservedParams
            )
            val createdAt = System.currentTimeMillis() / 1000L
            val eventInfo = EventInfo(id, trackingDataModel, createdAt)

            EventWorker.logEvent(ctx, eventInfo)
        }

        private fun getCategory(params: Map<String, String>): String {
            val category = params[Param.EA_CATEGORY]
            if (TextUtils.isEmpty(category)) return "unknown"
            return category!!
        }

        private fun getClientId(ctx: Context): String =
            InjectorUtil.provideRepository(ctx).getClientId()

        private fun getSourceId(ctx: Context, params: Map<String, String>): String {
            val appMetaData = Utils.getAppMetadata(ctx)
            val sourceId = params[Param.EA_SOURCE_ID]
            if (TextUtils.isEmpty(sourceId)) return appMetaData?.getString("erogames_analytics_source_id")!!
            return sourceId!!
        }

        private fun getTrackId(ctx: Context): String =
            TrackProviderUtil.getOrCreateTrackInfo(ctx).id

        private fun extractNonReservedParams(params: Map<String, String>): Map<String, String> {
            return params.filter { param ->
                var startWith = false
                RESERVED_PREFIXES.forEach {
                    if (param.key.startsWith(it)) startWith = true
                }
                startWith.not()
            }
        }
    }

    @Keep
    class Param {
        companion object {
            const val EA_CATEGORY: String = "ea_category"
            const val EA_SOURCE_ID: String = "ea_source_id"
        }
    }
}