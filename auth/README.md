# Erogames Auth Library

The library for user authentication via Erogames API.

## Integrating into the project

To add this library to the project:

1. Add repository in your project-level `build.gradle`.
```groovy
allprojects {
    repositories {
        ...
        maven {
            url "https://gitlab.com/api/v4/projects/20889762/packages/maven"
        }
    }
}
```

2. Add the following dependency in your app-level `build.gradle`.
```groovy
dependencies {
    ...
    implementation "com.erogames.auth:auth:<x.y.z>"
}
```
The latest version can be taken on the [tags page](https://gitlab.com/sky-network/erogames-android-sdk/-/tags).  
If the tag name is `auth-v1.5.0` then `<x.y.z>` is `1.5.0`.

3. Initialize the Erogames Auth before use:
```kotlin
ErogamesAuth.init(context, your_client_id)
```

## Usage

### Log in:
```kotlin
private val authResult =
    registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
      if (it.resultCode == Activity.RESULT_OK) onAuthResult(it.data)
    }
authResult.launch(ErogamesAuth.getLoginIntent(this))
```

Optionally:
```kotlin
ErogamesAuth.login(context)
```
To get the authentication result, override `onActivityResult` method of your Activity:
```kotlin
override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK) {
        when (requestCode) {
            ErogamesAuth.REQUEST_CODE_AUTH -> {
                // Get error message in case of unsuccessful authentication
                val authError = data?.getStringExtra(ErogamesAuth.EXTRA_ERROR)
                if (authError == null) {
                    val user = ErogamesAuth.getUser(this)
                } else {
                    // show error
                }
            }
            ErogamesAuth.REQUEST_CODE_LOGOUT -> Unit
        }
    }
}
```

### Sign up:
```kotlin
private val authResult =
    registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
      if (it.resultCode == Activity.RESULT_OK) onAuthResult(it.data)
    }
authResult.launch(ErogamesAuth.getSignupIntent(this))
```

Optionally:
```kotlin
ErogamesAuth.signup(context)
```
To get the authentication result, override `onActivityResult` method of your Activity:
```kotlin
override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK) {
        when (requestCode) {
            ErogamesAuth.REQUEST_CODE_AUTH -> {
                // Get error message in case of unsuccessful authentication
                val authError = data?.getStringExtra(ErogamesAuth.EXTRA_ERROR)
                if (authError == null) {
                    val user = ErogamesAuth.getUser(this)
                } else {
                    // show error
                }
            }
            ErogamesAuth.REQUEST_CODE_LOGOUT -> Unit
        }
    }
}
```

### Whitelabel info (base URL, logo URL, "buy Erogold" URL, authorize URL, etc.):
```kotlin
ErogamesAuth.getWhitelabel(context)
```
In case a user is not logged in yet but you need some of those Whitelabel info
(or ErogamesAuth.getWhitelabel(context) returns null):
```kotlin
ErogamesAuth.loadWhitelabel(context)
```

### Log out:
```kotlin
private val logoutResult =
    registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {}
val webLogout = true // if true, the user will log out of the site as well.
logoutResult.launch(ErogamesAuth.getLogoutIntent(this, webLogout))
```

Optionally:
```kotlin
val webLogout = true // if true, the user will log out of the site as well.
ErogamesAuth.logout(context, webLogout)
```

### Get current user:
```kotlin
ErogamesAuth.getUser(context)
```

### Reload user:
```kotlin
ErogamesAuth.reloadUser(context, listener)
```

### Get token:
```kotlin
ErogamesAuth.getToken(context)
```

### Refresh token:
```kotlin
ErogamesAuth.refreshToken(context, listener)
```

### Proceed payment:
```kotlin
val paymentId = "payment_uuid"
val amount = 1
ErogamesAuth.proceedPayment(context, paymentId, amount, listener)
```

### Load current quest info:
```kotlin
ErogamesAuth.loadCurrentQuest(context, listener)
```

### Load payment information:
```kotlin
ErogamesAuth.loadPaymentInfo(context, "payment_uuid", listener)
```

### Check for game updates:
```kotlin
ErogamesAuth.hasUpdate(context, listener)
```

### Show the game in the app store. If not available, open the game web page:
```kotlin
ErogamesAuth.openInAppStore(context, listener)
```
