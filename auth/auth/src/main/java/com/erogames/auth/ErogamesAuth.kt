package com.erogames.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.erogames.auth.model.*
import com.erogames.auth.ui.ApiIssueDialog
import com.erogames.auth.ui.AuthCallbackActivity
import com.erogames.auth.util.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

@Keep
object ErogamesAuth {
    private const val TAG = "ErogamesAuth"
    const val REQUEST_CODE_AUTH = 0x100
    const val REQUEST_CODE_LOGOUT = 0x101
    const val EXTRA_ERROR = "${Constants.PACKAGE_NAME_PREFIX}.extra.ERROR"
    const val EXTRA_USER = "${Constants.PACKAGE_NAME_PREFIX}.extra.USER"

    /**
     * ErogamesAuth initialization.
     *
     * @param ctx [Context]
     * @param clientId
     */
    @JvmStatic
    fun init(ctx: Context, clientId: String) {
        Log.i(TAG, "v${BuildConfig.VERSION_NAME}")
        InjectorUtil.provideRepository(ctx).setClientId(clientId.trim())
    }

    /**
     * Starts sign up process. The corresponding "signup" web URL will be opened in the default browser.
     * The Custom Chrome Tabs can be used if available.
     * To get authentication result, use [REQUEST_CODE_AUTH] request code in overridden [Activity.onActivityResult] method.
     *
     * @param ctx [Activity]
     * @param locale The preferable language of the "signup" web page.
     * By default, [Locale.ENGLISH] will be used.
     */
    @JvmStatic
    fun signup(ctx: Activity, locale: Locale = Locale.ENGLISH) {
        val repo = InjectorUtil.provideRepository(ctx)
        repo.setLocale(locale)
        AuthCallbackActivity.signupAction(ctx)
    }

    @JvmStatic
    fun getSignupIntent(ctx: Context, locale: Locale = Locale.ENGLISH): Intent {
        val repo = InjectorUtil.provideRepository(ctx)
        repo.setLocale(locale)
        return AuthCallbackActivity.getSignupIntent(ctx)
    }

    /**
     * Starts sign in process. The corresponding "sign in" web URL will be opened in the default browser.
     * The Custom Chrome Tabs can be used if available.
     * To get authentication result, use [REQUEST_CODE_AUTH] request code in overridden [Activity.onActivityResult] method.
     *
     * @param ctx [Activity]
     * @param locale The preferable language of the "signup" web page.
     * By default, [Locale.ENGLISH] will be used.
     */
    @JvmStatic
    fun login(ctx: Activity, locale: Locale = Locale.ENGLISH) {
        val repo = InjectorUtil.provideRepository(ctx)
        repo.setLocale(locale)
        AuthCallbackActivity.loginAction(ctx)
    }

    @JvmStatic
    fun getLoginIntent(ctx: Context, locale: Locale = Locale.ENGLISH): Intent {
        val repo = InjectorUtil.provideRepository(ctx)
        repo.setLocale(locale)
        return AuthCallbackActivity.getLoginIntent(ctx)
    }

    /**
     * Clear all local authentication data.
     * If [webLogout] is true then the corresponding "log out" web URL will be opened in the default browser.
     * In this case, the user will also be logged out from the web site.
     * To get log out result, use [REQUEST_CODE_LOGOUT] request code in overridden [Activity.onActivityResult] method.
     *
     * @param ctx [Activity]
     * @param webLogout Set to true for log out from the web site as well.
     */
    @JvmStatic
    @JvmOverloads
    fun logout(ctx: Activity, webLogout: Boolean = false) {
        AuthCallbackActivity.logoutAction(ctx, webLogout)
    }

    @JvmStatic
    fun getLogoutIntent(ctx: Activity, webLogout: Boolean = false) =
        AuthCallbackActivity.getLogoutIntent(ctx, webLogout)

    /**
     * Returns the current user data.
     *
     * @param ctx [Context]
     * @return The current user data, or null if the user is not logged in.
     */
    @JvmStatic
    fun getUser(ctx: Context): User? =
        InjectorUtil.provideRepository(ctx).getUser()

    /**
     * Refreshes the current user data.
     * To get the refreshed user data use {@link #getUser(Context)}
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun reloadUser(ctx: Context, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val user: User? = withContext(Dispatchers.IO) {
                try {
                    InjectorUtil.provideRepository(ctx).reloadUser()
                } catch (e: Exception) {
                    Log.e(TAG, "reloadUser", e)
                    t = e
                    null
                }
            }
            when {
                user != null -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Returns the current token.
     *
     * @param ctx [Context]
     * @return The the current token, or null if the user is not logged in.
     */
    @JvmStatic
    fun getToken(ctx: Context): Token? =
        InjectorUtil.provideRepository(ctx).getToken()

    /**
     * Refreshes the current token. For example, can be used if the token is expired.
     * To get the refreshed token use {@link #getToken(Context)}
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun refreshToken(ctx: Context, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val token: Token? = withContext(Dispatchers.IO) {
                try {
                    InjectorUtil.provideRepository(ctx).refreshToken()
                } catch (e: Exception) {
                    Log.e(TAG, "refreshToken", e)
                    t = e
                    null
                }
            }
            when {
                token != null -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Returns the whitelabel info.
     *
     * @param ctx [Context]
     * @return The the current [Whitelabel], or null if the user is not logged in.
     */
    @JvmStatic
    fun getWhitelabel(ctx: Context): Whitelabel? =
        InjectorUtil.provideRepository(ctx).getWhitelabel()

    /**
     * Load the whitelabel. For example, can be used if the user is not logged in.
     * To get the local whitelabel use {@link #getWhitelabel(Context)}
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun loadWhitelabel(ctx: Context, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val wl: Whitelabel? = withContext(Dispatchers.IO) {
                try {
                    InjectorUtil.provideRepository(ctx).loadWhitelabel()
                } catch (e: Exception) {
                    Log.e(TAG, "loadWhitelabel", e)
                    t = e
                    null
                }
            }
            when {
                wl != null -> listener.onSuccess(null)
                else -> {
                    if (t is ApiOutdatedException) ApiIssueDialog.show(ctx)
                    listener.onFailure(t)
                }
            }
        }
    }

    /**
     * Proceed a new payment.
     *
     * @param ctx [Context]
     * @param paymentId
     * @param amount, the amount in Erogold
     * @param listener [OnResult]
     */
    @JvmStatic
    fun proceedPayment(ctx: Context, paymentId: String?, amount: Int?, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val baseStatusResp: BaseStatusResp? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideRepository(ctx)
                try {
                    repo.proceedPayment(paymentId, amount)
                } catch (e: Exception) {
                    Log.e(TAG, "proceedPayment", e)
                    t = e
                    null
                }
            }
            when {
                baseStatusResp != null -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Loads current quest info
     *
     * Load key quest statistics and fields such as quest title and
     * description. The data returned also has information about the current ranking of
     * user's clan (user is loaded based on used auth token) and also current user's
     * individual contribution to the clan score. This endpoint allows implementing in-game
     * quest status widgets for more immersive gameplay.
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun loadCurrentQuest(ctx: Context, listener: OnResult<QuestData>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val questData: QuestData? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideRepository(ctx)
                try {
                    repo.getCurrentQuestData()
                } catch (e: Exception) {
                    Log.e(TAG, "loadCurrentQuest", e)
                    t = e
                    null
                }
            }
            when {
                questData != null -> listener.onSuccess(questData)
                else -> listener.onFailure(t)
            }
        }
    }

    @JvmStatic
    fun loadPaymentInfo(ctx: Context, paymentId: String, listener: OnResult<PaymentInfo>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val paymentInfo: PaymentInfo? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideRepository(ctx)
                try {
                    repo.getPaymentInfo(paymentId)
                } catch (e: Exception) {
                    Log.e(TAG, "loadPaymentInfo", e)
                    t = e
                    null
                }
            }
            when {
                paymentInfo != null -> listener.onSuccess(paymentInfo)
                else -> listener.onFailure(t)
            }
        }
    }

    @JvmStatic
    fun hasUpdate(ctx: Context, listener: OnResult<Boolean>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val hasUpdate: Boolean? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideRepository(ctx)
                try {
                    repo.hasUpdate(ctx.packageName, ctx.versionName())
                } catch (e: Exception) {
                    Log.e(TAG, "hasUpdate", e)
                    t = e
                    null
                }
            }
            when {
                hasUpdate != null -> listener.onSuccess(hasUpdate)
                else -> listener.onFailure(t)
            }
        }
    }

    @JvmStatic
    fun openInAppStore(ctx: Context, listener: OnResult<Boolean>) {
        var t: Throwable? = null
        val repo = InjectorUtil.provideRepository(ctx)

        getCoroutineScope(ctx).launch {
            try {
                val wl = repo.getOrLoadWhitelabel()
                val appStoreId = wl.androidAppStoreId ?: "com.erogames.app"
                val appStoreGameUri = Uri.parse("$appStoreId://details?id=${ctx.packageName}")
                val intent = Intent(Intent.ACTION_VIEW, appStoreGameUri)
                intent.flags = (Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                if (ctx.tryStartActivity(intent)) {
                    listener.onSuccess(true)
                    return@launch
                }

                val gameItem = repo.getOrLoadGameItem(ctx.packageName)
                    ?: throw Exception("GameItem (${ctx.packageName}) not found.")
                var gameWebPath = gameItem.gamePage ?: ""
                if (gameWebPath.startsWith("/")) gameWebPath = gameWebPath.drop(1)
                val webGameUri =
                    Uri.parse(wl.url).buildUpon().appendEncodedPath(gameWebPath).build()
                if (!ctx.tryStartActivity(Intent(Intent.ACTION_VIEW, webGameUri)))
                    t = Exception("Can neither launch game appstore nor open '$webGameUri'")
            } catch (e: Exception) {
                t = e
            }
            when {
                t != null -> listener.onFailure(t)
                else -> listener.onSuccess(true)
            }
        }
    }


    private fun getCoroutineScope(ctx: Context): CoroutineScope = when (ctx) {
        is AppCompatActivity -> ctx.lifecycleScope
        else -> CoroutineScope(Dispatchers.Main)
    }
}

interface OnResult<T> {
    fun onSuccess(data: T)
    fun onFailure(t: Throwable?)
}