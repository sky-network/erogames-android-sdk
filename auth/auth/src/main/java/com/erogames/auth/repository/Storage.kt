package com.erogames.auth.repository

import android.content.SharedPreferences
import com.erogames.auth.model.GameItem
import com.erogames.auth.model.Token
import com.erogames.auth.model.User
import com.erogames.auth.model.Whitelabel
import com.erogames.auth.util.InjectorUtil
import kotlinx.serialization.encodeToString
import java.util.Locale

/**
 * Local storage. Basically, this is a [SharedPreferences] wrapper.
 */
internal class Storage(private val prefs: SharedPreferences) {

    /**
     * Save a [User] to local storage
     * @param user
     */
    fun setUser(user: User) = prefs.edit().putString(
        PREF_USER, InjectorUtil.myJson.encodeToString(user)
    ).apply()

    /**
     * Returns a [User]
     */
    fun getUser(): User? {
        val userStr = prefs.getString(PREF_USER, null)
        userStr?.let { return InjectorUtil.myJson.decodeFromString<User>(it) }
        return null
    }

    /**
     * Removes a [User] from local storage
     */
    fun removeUser() = prefs.edit().remove(PREF_USER).apply()

    /**
     * Save a [Token] to local storage
     * @param token
     */
    fun setToken(token: Token) = prefs.edit().putString(
        PREF_TOKEN, InjectorUtil.myJson.encodeToString(token)
    ).apply()

    /**
     * Returns a [Token]
     */
    fun getToken(): Token? {
        val tokenStr = prefs.getString(PREF_TOKEN, null)
        tokenStr?.let { return InjectorUtil.myJson.decodeFromString<Token>(it) }
        return null
    }

    /**
     * Remove [Token] from local storage
     */
    fun removeToken() = prefs.edit().remove(PREF_TOKEN).apply()

    /**
     * Save a preferred [Locale] to local storage
     * @param locale
     */
    fun setLocale(locale: Locale) = prefs.edit().putString(
        PREF_LOCALE, locale.language
    ).apply()

    /**
     * Returns a preferred [Locale]
     */
    fun getLocale(): Locale {
        val language = prefs.getString(PREF_LOCALE, null) ?: return Locale.ENGLISH
        return Locale(language)
    }

    /**
     * Save [Whitelabel] to local storage
     * @param whitelabel
     */
    fun setWhitelabel(whitelabel: Whitelabel) = prefs.edit().putString(
        PREF_WHITELABEL, InjectorUtil.myJson.encodeToString(whitelabel)
    ).apply()

    /**
     * Returns [Whitelabel] form local storage
     */
    fun getWhitelabel(): Whitelabel? {
        val json = prefs.getString(PREF_WHITELABEL, null)
        json?.let { return InjectorUtil.myJson.decodeFromString<Whitelabel>(it) }
        return null
    }

    /**
     * Removes [Whitelabel] from local storage
     */
    fun removeWhitelabel() = prefs.edit().remove(PREF_WHITELABEL).apply()

    /**
     * Save client id to local storage
     * @param clientId
     */
    fun setClientId(clientId: String) = prefs.edit().putString(PREF_CLIENT_ID, clientId).apply()

    /**
     * Returns client id
     */
    fun getClientId(): String = prefs.getString(PREF_CLIENT_ID, "")!!

    /**
     * Save Api Update Url to local storage
     * @param apiUpdateUrl
     */
    fun setApiUpdateUrl(apiUpdateUrl: String) =
        prefs.edit().putString(PREF_API_UPDATE_URL, apiUpdateUrl).apply()

    /**
     * Returns Api Update Url
     */
    fun getApiUpdateUrl(): String? = prefs.getString(PREF_API_UPDATE_URL, null)

    fun setAccessKey(accessKey: String) =
        prefs.edit().putString(PREF_API_ACCESS_KEY, accessKey).apply()

    fun getAccessKey(): String = prefs.getString(PREF_API_ACCESS_KEY, "")!!

    fun setWhitelabelId(whitelabelId: String?) =
        prefs.edit().putString(PREF_API_WHITELABEL_ID, whitelabelId).apply()

    fun getWhitelabelId(): String = prefs.getString(PREF_API_WHITELABEL_ID, "erogames")!!

    fun setGameItem(gameItem: GameItem) = prefs.edit().putString(
        PREF_GAME_ITEM, InjectorUtil.myJson.encodeToString(gameItem)
    ).apply()

    fun getGameItem(): GameItem? {
        val json = prefs.getString(PREF_GAME_ITEM, null)
        json?.let { return InjectorUtil.myJson.decodeFromString<GameItem>(it) }
        return null
    }

    fun removeGameItem() = prefs.edit().remove(PREF_GAME_ITEM).apply()

    companion object {
        private const val PREF_USER = "com_erogames_sdk_pref_user"
        private const val PREF_TOKEN = "com_erogames_sdk_pref_token"
        private const val PREF_WHITELABEL = "com_erogames_sdk_pref_whitelabel"
        private const val PREF_LOCALE = "com_erogames_sdk_pref_locale"
        private const val PREF_CLIENT_ID = "com_erogames_sdk_pref_client_id"
        private const val PREF_API_UPDATE_URL = "com_erogames_sdk_pref_api_update_url"
        private const val PREF_API_WHITELABEL_ID = "com_erogames_sdk_pref_api_whitelabel_id"
        private const val PREF_API_ACCESS_KEY = "com_erogames_sdk_pref_api_access_key"
        private const val PREF_GAME_ITEM = "com_erogames_sdk_pref_game_item"
    }
}