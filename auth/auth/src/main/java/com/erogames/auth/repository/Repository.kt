package com.erogames.auth.repository

import com.erogames.auth.api.ApiService
import com.erogames.auth.model.*
import com.erogames.auth.util.ApiOutdatedException
import com.erogames.auth.util.Constants
import net.swiftzer.semver.SemVer
import java.util.*

internal class Repository(
    accessKey: String,
    whitelabelId: String,
    private val apiService: ApiService,
    private val storage: Storage
) {

    init {
        storage.setAccessKey(accessKey)
        storage.setWhitelabelId(whitelabelId)
    }

    /**
     * Return JWT token from an API service.
     */
    private suspend fun loadJwtToken(accessKey: String): String =
        apiService.getJwtToken(accessKey).token

    private fun getAccessKey(): String = storage.getAccessKey()

    private fun getWhitelabelId(): String = storage.getWhitelabelId()

    suspend fun loadWhitelabel(): Whitelabel {
        val whitelabelId = getWhitelabelId()
        val whitelabels = loadWhitelabels()
        val whitelabel = whitelabels.find { it.slug == whitelabelId }
            ?: throw NullPointerException("The whitelabel '$whitelabelId' does not exist.")
        setWhitelabel(whitelabel)
        return whitelabel
    }

    /**
     * Return a [Whitelabel] list from an API service.
     */
    private suspend fun loadWhitelabels(): List<Whitelabel> {
        val whitelabelId = getWhitelabelId()
        val jwtToken = loadJwtToken(getAccessKey())
        val whitelabelsResp = apiService.getWhitelabels(jwtToken)
        val minVersion: Float = whitelabelsResp.minVersion.toFloat()

        if (Constants.API_MIN_VERSION.toFloat() < minVersion) {
            val whitelabel = whitelabelsResp.whitelabels.find { it.slug == whitelabelId }
            whitelabel?.let { storage.setApiUpdateUrl(whitelabel.url) }
            throw ApiOutdatedException("API is outdated. Current version: ${Constants.API_MIN_VERSION}. Actual version: $minVersion.")
        }

        return whitelabelsResp.whitelabels
    }

    /**
     * Save a [Token] into local storage.
     */
    private fun setToken(token: Token) = storage.setToken(token)

    /**
     * Return a [Token] from local storage.
     */
    fun getToken() = storage.getToken()

    private suspend fun getOrRefreshToken(): Token? {
        val token = storage.getToken()
        if (token != null && token.isExpired()) return refreshToken()
        return token
    }

    /**
     * Return a [Token] from an API service.
     */
    suspend fun loadToken(
        code: String?,
        grantType: String?,
        redirectUri: String?,
        codeVerifier: String?,
    ): Token {
        val whitelabel = getWhitelabel()
            ?: throw NullPointerException("The whitelabel can't be null.")

        val token = apiService.getToken(
            clientId = getClientId(),
            code = code,
            grantType = grantType,
            redirectUri = redirectUri,
            codeVerifier = codeVerifier,
            tokenUrl = whitelabel.tokenUrl
        )
        setToken(token)
        return token
    }

    /**
     * Refresh a [Token].
     * A refreshed token retrieved from API service will be saved locally.
     */
    suspend fun refreshToken(): Token {
        val token = getToken()
        val whitelabel = getWhitelabel()
        if (token == null) throw NullPointerException("The token can't be null.")
        if (whitelabel == null) throw NullPointerException("The whitelabel can't be null.")

        val refreshedToken = apiService.refreshToken(
            tokenUrl = whitelabel.tokenUrl,
            clientId = getClientId(),
            refreshToken = token.refreshToken
        )
        setToken(refreshedToken)
        return refreshedToken
    }

    /**
     * Set client id to local storage.
     * @param clientId
     */
    fun setClientId(clientId: String) = storage.setClientId(clientId)

    /**
     * Get client id from local storage.
     * @return clientId
     */
    fun getClientId(): String = storage.getClientId()

    /**
     * Remove a token from local storage.
     */
    private fun removeToken() = storage.removeToken()

    /**
     * Save a [User] into local storage.
     */
    private fun setUser(user: User) = storage.setUser(user)

    /**
     * Return a [User] from local storage.
     */
    fun getUser(): User? = storage.getUser()

    /**
     * Return a [User] from an API service.
     * @param profileUrl By default, this value will be taken from the [Whitelabel].
     */
    suspend fun loadUser(profileUrl: String? = null): User {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        var whitelabel: Whitelabel? = null
        if (profileUrl == null) {
            whitelabel = getWhitelabel()
                ?: throw NullPointerException("The whitelabel can't be null.")
        }

        val user = apiService.getUser(
            profileUrl ?: whitelabel!!.profileUrl,
            "Bearer ${token.accessToken}"
        ).user
        setUser(user)
        return user
    }

    /**
     * Reload a [User] data.
     * A reloaded user data retrieved from API service will be saved locally.

     * @see getUser
     */
    suspend fun reloadUser(): User {
        val token = getOrRefreshToken()
        val whitelabel = getWhitelabel()
        if (token == null || whitelabel == null) throw NullPointerException("The token and/or whitelabel can't be null.")

        val user = apiService.getUser(whitelabel.profileUrl, "Bearer ${token.accessToken}").user
        setUser(user)
        return user
    }

    /**
     * Remove a user from local storage.
     */
    private fun removeUser() = storage.removeUser()

    /**
     * Save a [Whitelabel] into local storage.
     */
    private fun setWhitelabel(whitelabel: Whitelabel) = storage.setWhitelabel(whitelabel)

    /**
     * Return a [Whitelabel] from local storage.
     */
    fun getWhitelabel(): Whitelabel? = storage.getWhitelabel()

    suspend fun getOrLoadWhitelabel() = storage.getWhitelabel() ?: loadWhitelabel()

    /**
     * Remove a [Whitelabel] from local storage.
     */
    private fun removeWhitelabel() = storage.removeWhitelabel()

    private fun removeGameItem() = storage.removeGameItem()

    /**
     * Save a [Locale] into local storage.
     */
    fun setLocale(locale: Locale) =
        when {
            Constants.SUPPORTED_LANGUAGES.contains(locale.language) -> storage.setLocale(locale)
            else -> storage.setLocale(Locale.ENGLISH)
        }

    /**
     * Return a [Locale] from local storage.
     */
    fun getLocale() = storage.getLocale()

    /**
     * Clear all local data: [User], [Token], [Whitelabel]
     */
    fun clearLocalData() {
        removeUser()
        removeToken()
        removeWhitelabel()
        removeGameItem()
    }

    /**
     * Proceed payment.
     * @param paymentId
     * @param amount
     * @return [BaseStatusResp]
     */
    suspend fun proceedPayment(paymentId: String?, amount: Int?): BaseStatusResp {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        val whitelabel = getWhitelabel() ?: throw NullPointerException("Whitelabel can't be null.")

        return apiService.proceedPayment(
            paymentUrl = whitelabel.url + "/api/v1/payments",
            accessToken = "Bearer ${token.accessToken}",
            paymentId = paymentId,
            amount = amount
        )
    }

    /**
     * Get current quest data
     * @return [QuestData]
     */
    suspend fun getCurrentQuestData(): QuestData {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        val whitelabel = getWhitelabel() ?: throw NullPointerException("Whitelabel can't be null.")

        return apiService.getCurrentQuestData(
            url = whitelabel.url + Constants.QUEST_ENDPOINT,
            accessToken = "Bearer ${token.accessToken}"
        )
    }

    /**
     * Get payment information
     * @return [PaymentInfo]
     */
    suspend fun getPaymentInfo(paymentId: String): PaymentInfo {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        val whitelabel = getWhitelabel() ?: throw NullPointerException("Whitelabel can't be null.")

        return apiService.getPaymentInfo(
            url = whitelabel.url + String.format(Constants.PAYMENT_INFO_ENDPOINT, paymentId),
            accessToken = "Bearer ${token.accessToken}"
        )
    }

    suspend fun hasUpdate(appId: String, currentVersionName: String): Boolean {
        val item = getOrLoadGameItem(appId)
        if (item?.rawVersionCode == null) return false

        val currentVersion: SemVer = SemVer.parse(currentVersionName)
        val serverVersion: SemVer = SemVer.parse(item.rawVersionCode)
        return serverVersion.compareTo(currentVersion) == 1
    }

    suspend fun getOrLoadGameItem(appId: String) = storage.getGameItem() ?: loadGameItem(appId)

    private suspend fun loadGameItem(appId: String): GameItem? {
        val gameItem = getGameItems().find { it.appId == appId }
        if (gameItem != null) setGameItem(gameItem)
        return gameItem
    }

    private fun setGameItem(gameItem: GameItem) = storage.setGameItem(gameItem)

    private suspend fun getGameItems(): List<GameItem> {
        val jwtToken = loadJwtToken(getAccessKey())
        val whitelabel = getOrLoadWhitelabel()
        return apiService.getGameItems(
            url = whitelabel.url + Constants.GAME_ITEMS_ENDPOINT,
            jwtToken = jwtToken
        ).items
    }
}