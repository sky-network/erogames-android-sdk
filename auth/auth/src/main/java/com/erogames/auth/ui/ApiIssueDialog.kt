package com.erogames.auth.ui

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.erogames.auth.R
import com.erogames.auth.util.InjectorUtil
import com.erogames.auth.util.Utils
import com.erogames.auth.util.showToast

class ApiIssueDialog : DialogFragment() {

    @Deprecated("Deprecated in Java")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val apiUpdateUrl = InjectorUtil.provideStorage(requireContext()).getApiUpdateUrl()
        val message =
            getString(R.string.com_erogames_sdk_api_issue_dialog_message, apiUpdateUrl)

        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.com_erogames_sdk_api_issue_dialog_title)
            .setMessage(message)
            .setPositiveButton(
                R.string.com_erogames_sdk_api_issue_dialog_pos_button
            ) { _, _ -> Utils.launchUrl(requireContext(), apiUpdateUrl!!) }
            .create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.com_erogames_sdk_color_accent
                    )
                )
        }
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (arguments?.getBoolean(ARG_FINISH_ON_DISMISS, false) == true) {
            activity?.finish()
        }
    }

    companion object {

        private const val TAG = "ApiIssueDialog"
        private const val ARG_FINISH_ON_DISMISS = "arg_finish_on_dismiss"

        /**
         * Display AlertDialog.
         * If [Context] is not an [Activity] then simple [Toast] will be displayed.
         *
         *@param ctx [Context]
         *@param finishOnDismiss Finish [Activity] "on dismiss" event if true.
         */
        fun show(ctx: Context, finishOnDismiss: Boolean = false) {
            if (ctx is FragmentActivity && ctx.supportFragmentManager.findFragmentByTag(TAG) == null) {
                val dialog = ApiIssueDialog()
                dialog.arguments = bundleOf(Pair(ARG_FINISH_ON_DISMISS, finishOnDismiss))
                dialog.show(ctx.supportFragmentManager, TAG)
            } else {
                val apiUpdateUrl = InjectorUtil.provideStorage(ctx).getApiUpdateUrl()
                val message =
                    ctx.getString(R.string.com_erogames_sdk_api_issue_dialog_message, apiUpdateUrl)
                ctx.showToast(message, Toast.LENGTH_LONG)
            }
        }
    }
}