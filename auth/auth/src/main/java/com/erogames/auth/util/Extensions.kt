package com.erogames.auth.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.util.Log
import android.widget.Toast
import java.security.MessageDigest
import java.util.Locale


/**
 * Returns SHA-256 hash of string
 */
internal fun String.sha256(): ByteArray = hash("SHA-256")

/**
 * Return hash of string.
 * @param algorithm
 */
internal fun String.hash(algorithm: String): ByteArray = MessageDigest
    .getInstance(algorithm)
    .digest(toByteArray())

/**
 * Returns localized string.
 * @param resId
 * @param locale
 */
internal fun Context.getLocalizedString(resId: Int, locale: Locale): String {
    val configuration = Configuration(resources.configuration)
    configuration.setLocale(locale)
    return createConfigurationContext(configuration).getString(resId)
}

/**
 * Show [Toast]
 * @param message
 * @param duration
 */
internal fun Context.showToast(message: String?, duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(this, message, duration).show()

internal fun Context.versionName() = getPackageInfoCompat(packageName, 0).versionName

internal fun Context.tryStartActivity(intent: Intent) = try {
    startActivity(intent)
    true
} catch (e: Exception) {
    Log.w("ErogamesAuth", "tryStartActivity", e)
    false
}

internal fun Context.getMetaDataString(key: String) = getApplicationInfoCompat(
    packageName,
    PackageManager.GET_META_DATA
).metaData?.getString(key)

internal fun Activity.setResultAndFinish(resultCode: Int, data: Intent? = null) {
    setResult(resultCode, data)
    finish()
}

fun Context.getPackageInfoCompat(packageName: String, flags: Int = 0): PackageInfo =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        packageManager.getPackageInfo(
            packageName,
            PackageManager.PackageInfoFlags.of(flags.toLong())
        )
    } else {
        @Suppress("DEPRECATION")
        packageManager.getPackageInfo(packageName, flags)
    }

fun Context.getApplicationInfoCompat(packageName: String, flags: Int = 0): ApplicationInfo {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        packageManager.getApplicationInfo(
            packageName,
            PackageManager.ApplicationInfoFlags.of(flags.toLong())
        )
    } else {
        @Suppress("DEPRECATION")
        packageManager.getApplicationInfo(packageName, flags)
    }
}