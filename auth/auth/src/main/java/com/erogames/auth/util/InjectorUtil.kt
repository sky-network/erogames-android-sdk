package com.erogames.auth.util

import android.content.Context
import androidx.preference.PreferenceManager
import com.erogames.auth.R
import com.erogames.auth.api.ApiService
import com.erogames.auth.repository.Repository
import com.erogames.auth.repository.Storage
import kotlinx.serialization.json.Json

/**
 * Simple injector.
 */
internal object InjectorUtil {
    private var repo: Repository? = null
    private var storage: Storage? = null
    private var apiService: ApiService? = null
    val myJson: Json by lazy { Json { ignoreUnknownKeys = true } }

    @JvmStatic
    fun provideApiService(context: Context) =
        apiService ?: ApiService.create(context.applicationContext).also { apiService = it }

    @JvmStatic
    fun provideRepository(context: Context): Repository {
        if (repo != null) return repo!!
        val accessKey = context.getString(R.string.com_erogames_sdk_access_key)
        val whitelabelId = context.getMetaDataString("erogames_auth_whitelabel_id")!!
        return repo ?: Repository(
            accessKey,
            whitelabelId,
            provideApiService(context.applicationContext),
            provideStorage(context.applicationContext)
        ).also {
            if (whitelabelId != it.getWhitelabel()?.slug) it.clearLocalData()
            repo = it
        }
    }

    @JvmStatic
    fun provideStorage(context: Context) = storage ?: Storage(
        PreferenceManager.getDefaultSharedPreferences(context)
    ).also { storage = it }
}