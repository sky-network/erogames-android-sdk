package com.erogames.auth.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.erogames.auth.model.Whitelabel
import com.erogames.auth.repository.Repository
import com.erogames.auth.util.AuthUtil
import com.erogames.auth.util.Pkce
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.util.*

internal class AuthCallbackViewModel(
    private val repository: Repository,
    private val codeVerifier: String,
    private val redirectUri: String
) : ViewModel() {

    private val _authState = MutableStateFlow(AuthState())
    val authState: StateFlow<AuthState> = _authState.asStateFlow()

    fun startAuth(signup: Boolean = false) {
        viewModelScope.launch {
            try {
                val whitelabel = repository.loadWhitelabel()
                val authUrl = buildAuthUrl(whitelabel.authorizeUrl, signup)
                _authState.update { it.copy(authUrl = authUrl) }
            } catch (e: Exception) {
                _authState.update { it.copy(error = e) }
            }
        }
    }

    /**
     * Set code and proceed authentication flow
     */
    fun proceedAuth(code: String) {
        _authState.update { it.copy(authUrl = null) }
        viewModelScope.launch {
            try {
                repository.loadToken(code, "authorization_code", redirectUri, codeVerifier)
                repository.loadUser()
                _authState.update { it.copy(signedIn = true) }
            } catch (e: Exception) {
                _authState.update { it.copy(signedIn = false, error = e) }
            }
        }
    }

    /**
     * Build authentication URL
     */
    private fun buildAuthUrl(authorizeUrl: String, forceRegistration: Boolean): String {
        val codeChallenge = Pkce.generateCodeChallenge(Pkce.getCodeVerifier())
        val clientId = repository.getClientId()
        val locale = repository.getLocale()

        return AuthUtil.buildAuthUrl(
            authorizeUrl, codeChallenge, redirectUri, clientId, locale, forceRegistration.toString()
        )
    }

    /**
     * Build log out URL
     */
    private fun buildWebLogoutUrl(): String? {
        val whitelabel = getWhitelabel()
        val accessToken = repository.getToken()?.accessToken
        if (whitelabel != null && accessToken != null) {
            return AuthUtil.buildWebLogoutUrl(
                whitelabel.url, accessToken, redirectUri
            )
        }
        return null
    }

    /**
     * Returns locally stored [Whitelabel]
     */
    private fun getWhitelabel(): Whitelabel? = repository.getWhitelabel()

    /**
     * Returns preferred [Locale]
     */
    fun getLocale(): Locale = repository.getLocale()

    /**
     * Log out trigger
     */
    fun logout() {
        val webLogoutUrl = buildWebLogoutUrl()
        clearLocalData()
        _authState.update { it.copy(signedOut = true, webLogoutUrl = webLogoutUrl) }
    }

    /**
     * Clear all local data
     */
    private fun clearLocalData() = repository.clearLocalData()
}