package com.erogames.auth.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class WLTheme(
    @SerialName("highlight_color")
    val highlightColor: String? = null,
    @SerialName("favicon_url")
    val faviconUrl: String? = null,
)