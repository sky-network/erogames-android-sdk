package com.erogames.auth.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class GameItem(
    @SerialName("app_id")
    val appId: String? = null,
    val versionCode: Int? = null,
    val rawVersionCode: String? = null,
    val siteVersionCode: Int? = null,
    @SerialName("game_page")
    val gamePage: String? = null
)