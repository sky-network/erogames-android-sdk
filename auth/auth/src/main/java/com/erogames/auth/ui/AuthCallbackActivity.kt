package com.erogames.auth.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.erogames.auth.ErogamesAuth
import com.erogames.auth.R
import com.erogames.auth.util.*
import kotlinx.coroutines.launch


internal class AuthCallbackActivity : AppCompatActivity() {

    private var authCode: String? = null
    private var startAuth: Boolean = false
    private var targetAction: String? = ACTION_LOGIN
    private val viewModel by viewModels<AuthCallbackViewModel> {
        AuthCallbackViewModelFactory(
            InjectorUtil.provideRepository(this),
            Pkce.getCodeVerifier(),
            AuthUtil.buildRedirectUri(this)
        )
    }
    private lateinit var actionTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth_callback)

        actionTextView = findViewById(R.id.auth_callback_action)

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.authState.collect { onAuthState(it) }
            }
        }

        targetAction = intent.action
        updateUI()
        when (intent.action) {
            ACTION_LOGIN, ACTION_SIGNUP -> {
                startAuth = true
                viewModel.startAuth(intent.action == ACTION_SIGNUP)
            }
            ACTION_LOGOUT -> {
                startAuth = true
                viewModel.logout()
            }
            else -> handleDeepLinks(intent)
        }
    }

    private fun onAuthState(state: AuthState) {
        state.error?.let {
            Log.e(TAG, "onAuthState", it)
            setErrorResult(it.message.toString())
            if (it is ApiOutdatedException) ApiIssueDialog.show(this, true)
            else finish()
        }?.run { return }

        if (state.signedOut) onSignedOut(state.webLogoutUrl).run { return }
        if (state.signedIn) {
            val intent = Intent()
            intent.putExtra(ErogamesAuth.EXTRA_USER, ErogamesAuth.getUser(this))
            setResultAndFinish(RESULT_OK, intent).run { return }
        }

        state.authUrl?.let { Utils.launchUrl(this, it).run { return } }
    }

    /**
     * Observes 'on logout' action.
     */
    private fun onSignedOut(webLogoutUrl: String?) {
        val webLogout = intent.getBooleanExtra(EXTRA_WEB_LOGOUT, false)
        if (webLogout.not() || webLogoutUrl == null) setResultAndFinish(RESULT_OK).run { return }
        Utils.launchUrl(this, webLogoutUrl)
    }

    private fun updateUI() {
        val actionRes = when (targetAction) {
            ACTION_LOGIN -> R.string.com_erogames_sdk_loggingin
            ACTION_SIGNUP -> R.string.com_erogames_sdk_signingup
            ACTION_LOGOUT -> R.string.com_erogames_sdk_signingout
            else -> R.string.com_erogames_sdk_empty_str
        }
        actionTextView.text = getLocalizedString(actionRes, viewModel.getLocale())
    }

    private fun setErrorResult(errMessage: String) {
        val intent = Intent()
        intent.putExtra(ErogamesAuth.EXTRA_ERROR, errMessage)
        setResult(RESULT_OK, intent)
    }

    override fun onResume() {
        super.onResume()
        if (!startAuth && TextUtils.isEmpty(authCode)) finish()
        startAuth = false
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleDeepLinks(intent)
    }

    private fun handleDeepLinks(intent: Intent) {
        intent.data?.let { uri ->
            Log.d(TAG, "Deep link: $uri")
            val isLogout = uri.getQueryParameter(LOGOUT_PARAM)
            if (isLogout != null) setResultAndFinish(RESULT_OK).run { return }

            authCode = uri.getQueryParameter(CODE_PARAM)
            authCode?.let { viewModel.proceedAuth(it) }
        }
    }

    companion object {

        private const val TAG = "AuthCallbackActivity"
        private const val ACTION_SIGNUP = "${Constants.PACKAGE_NAME_PREFIX}.action.SIGNUP"
        private const val ACTION_LOGIN = "${Constants.PACKAGE_NAME_PREFIX}.action.LOGIN"
        private const val ACTION_LOGOUT = "${Constants.PACKAGE_NAME_PREFIX}.action.API_LOGOUT"

        private const val CODE_PARAM = "code"
        private const val LOGOUT_PARAM = "logout"
        private const val EXTRA_WEB_LOGOUT = "web_logout"

        fun signupAction(ctx: Activity) {
            val i = Intent(ctx, AuthCallbackActivity::class.java)
            i.action = ACTION_SIGNUP
            ctx.startActivityForResult(i, ErogamesAuth.REQUEST_CODE_AUTH)
        }

        fun getSignupIntent(ctx: Context) =
            Intent(ctx, AuthCallbackActivity::class.java).apply { action = ACTION_SIGNUP }

        fun loginAction(ctx: Activity) {
            val i = Intent(ctx, AuthCallbackActivity::class.java)
            i.action = ACTION_LOGIN
            ctx.startActivityForResult(i, ErogamesAuth.REQUEST_CODE_AUTH)
        }

        fun getLoginIntent(ctx: Context) =
            Intent(ctx, AuthCallbackActivity::class.java).apply { action = ACTION_LOGIN }

        fun logoutAction(ctx: Activity, webLogout: Boolean) {
            val i = Intent(ctx, AuthCallbackActivity::class.java)
            i.action = ACTION_LOGOUT
            i.putExtra(EXTRA_WEB_LOGOUT, webLogout)
            ctx.startActivityForResult(i, ErogamesAuth.REQUEST_CODE_LOGOUT)
        }

        fun getLogoutIntent(ctx: Context, webLogout: Boolean) =
            Intent(ctx, AuthCallbackActivity::class.java).apply {
                action = ACTION_LOGOUT
                putExtra(EXTRA_WEB_LOGOUT, webLogout)
            }
    }
}