package com.erogames.auth.ui

data class AuthState(
    val signedIn: Boolean = false,
    val signedOut: Boolean = false,
    val webLogoutUrl: String? = null,
    val authUrl: String? = null,
    val error: Throwable? = null,
)