# Version 2.0.3 - Jul 14, 2023
* Updated project dependencies.
* Code cleanup.

# Version 2.0.2 - May 1, 2023
* Bug fixes.

# Version 2.0.1 - May 1, 2023
* Added 'QUERY_ALL_PACKAGES' permission.

# Version 2.0.0 - Dec 14, 2022
* Removed deprecated code.

# Version 1.5.5 - Dec 14, 2022
* Fixed minor bugs.

# Version 1.5.4 - Dec 14, 2022
* Replaced LiveData with Flow.
* Added the ability to use the Activity Result API.

# Version 1.5.3 - Dec 7, 2022
* Fixed incorrect game web url.

# Version 1.5.2 - Dec 7, 2022
* Added the ability to launch the appropriate app store according to the whitelabel.

# Version 1.5.1 - Dec 6, 2022
* Added the ability to show the game in the app store (if not available, open the game web page).

# Version 1.5.0 - Dec 5, 2022
* Added the ability to check for game updates.

# Version 1.4.13 - Nov 29, 2022
* Updated project dependencies.

# Version 1.4.12 - Nov 23, 2022
* Added Whitelabel theme.
* Deprecated the loginByPassword() and the registerUser() methods.

# Version 1.4.11 - Oct 24, 2022
## Updated
* AGP, project dependencies and target/compile SDK version.

# Version 1.4.10 - Jun 11, 2021
* Decreased 'kotlinx-serialization-json' version (due to 'kotlin_module conflict' issue on older Android Gradle plugin versions).

# Version 1.4.8 - Jun 10, 2021
## Changed
* The way to set the 'whitelabel id'. Now, this value can only be set in the AndroidManifest.xml file.
## Removed
* 'com_erogames_sdk_client_id' string resource.

# Version 1.4.7 - May 31, 2021
* Minor update.

# Version 1.4.6 - May 14, 2021
## Improved
* An access token will be refreshed automatically (during API request) if it's expired.

# Version 1.4.5 - May 14, 2021
## Replaced
* JCenter repository by Maven Central.

# Version 1.4.4 - Apr 24, 2021
## Added
* An ability to get payment information.

# Version 1.4.3 - Apr 20, 2021
## Removed
* Unused string resources.
## Updated
* Sample.
* README.md.
* Project gradle dependencies.

# Version 1.4.2 - Mar 30, 2021
## Added
* New fields to QuestData: 'bestPlayers' and 'userAttempt'.