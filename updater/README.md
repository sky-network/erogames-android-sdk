# Erogames Updater Library

## Integrating into the project

To add the library to the project:

1. Add repository in your project-level `build.gradle`.

```groovy
allprojects {
    repositories {
        maven {
            url "https://gitlab.com/api/v4/projects/20889762/packages/maven"
        }
    }
}
```

2. Add the following dependency in the app-level `build.gradle`.

```groovy
dependencies {
    implementation "com.erogames.updater:updater:<x.y.z>"
}
```

## Usage

### Init:

```kotlin
ErogamesUpdater.init(context, accessKey, whitelabelId, locale)
```

### Update:

```kotlin
ErogamesUpdater.update(context, listener)
```

### Stop:

```kotlin
ErogamesUpdater.stop(context)
```