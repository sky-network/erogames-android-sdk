package com.erogames.updater.sample

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.erogames.updater.ErogamesUpdater
import com.erogames.updater.service.UpdateListener
import com.erogames.updater.service.UpdateState
import com.erogames.updater.util.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Locale

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val permissions =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            arrayOf(Manifest.permission.POST_NOTIFICATIONS)
        else arrayOf()

    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            it.entries.firstOrNull { entry1 -> !entry1.value }?.let { entry2 ->
                Toast.makeText(this, "${entry2.key} is denied", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val accessKey = "<accessKey>"
        val whitelabelId = "erogames"
        val locale = Locale.ENGLISH
        ErogamesUpdater.init(applicationContext, accessKey, whitelabelId, locale)

        findViewById<View>(R.id.updateButton).setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                if (hasPermissions()) {
                    ErogamesUpdater.update(this@MainActivity, object : UpdateListener {
                        override fun onResult(state: UpdateState?) {
                            if (!isFinishing && !isDestroyed) {
                                handleUpdateState(state)
                            }
                        }
                    })
                } else {
                    permissionLauncher.launch(permissions)
                }
            }
        }

        findViewById<View>(R.id.cancelButton).setOnClickListener {
            ErogamesUpdater.stop(this)
        }
    }

    private fun hasPermissions(): Boolean {
        val atLeastTiramisu = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU
        val hasPermission = when {
            atLeastTiramisu -> hasPermission(Manifest.permission.POST_NOTIFICATIONS)
            else -> true
        }
        if (!hasPermission) {
            Log.w(TAG, "Permission 'POST_NOTIFICATIONS' is denied.")
        }
        return hasPermission
    }

    private fun handleUpdateState(state: UpdateState?) {
        Log.d(TAG, "onResult: $state")
    }

    companion object {
        const val TAG = "MainActivity"
    }
}