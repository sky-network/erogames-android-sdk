plugins {
    alias(libs.plugins.gradle.versions)
}

tasks.register<Delete>("clean").configure {
    delete(rootProject.buildDir)
}