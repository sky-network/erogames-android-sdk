plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    kotlin("plugin.serialization") version libs.versions.kotlin
    id("maven-publish")
}

android {
    namespace = "com.erogames.updater"
    compileSdk = libs.versions.compileSdk.get().toInt()

    defaultConfig {
        minSdk = libs.versions.minSdk.get().toInt()
        consumerProguardFiles("consumer-proguard-rules.pro")
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    sourceSets {
        getByName("main") {
            java.srcDir("src/main/kotlin")
            jniLibs.srcDirs("jniLibs")
        }
    }
}

dependencies {
    implementation(libs.androidx.core)
    implementation(libs.ktor.client.core)
    implementation(libs.ktor.client.okhttp)
    implementation(libs.ktor.client.negotiation)
    implementation(libs.ktor.client.serialization)
    implementation(libs.ktor.serialization.json)
    implementation(libs.squareup.okhttp3.logging.interceptor)
    implementation(libs.swiftzer.semver)
    implementation(libs.androidx.lifecycle.service)
    implementation(libs.androidx.preference.ktx)
    implementation(libs.erogames.patcher)
    implementation(libs.androidx.lifecycle.process)

    testImplementation(libs.junit4)
}

afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>("release") {
                groupId = "com.erogames.updater"
                artifactId = "updater"
                version = libs.versions.versionName.get()
                from(components["release"])
            }
        }

        repositories {
            maven {
                url = uri("https://gitlab.com/api/v4/projects/20889762/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Private-Token"
                    value = findProperty("gitLabPrivateToken") as String?
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}