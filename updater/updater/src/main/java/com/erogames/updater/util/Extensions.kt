package com.erogames.updater.util

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Parcelable
import android.service.notification.StatusBarNotification
import androidx.core.content.ContextCompat

internal fun Context.versionName() = getPackageInfoCompat(packageName, 0).versionName

fun Context.getPackageInfoCompat(packageName: String, flags: Int = 0): PackageInfo =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        packageManager.getPackageInfo(
            packageName,
            PackageManager.PackageInfoFlags.of(flags.toLong())
        )
    } else {
        packageManager.getPackageInfo(packageName, flags)
    }

fun Context.getPackageArchiveInfoCompat(packageName: String, flags: Int = 0): PackageInfo? =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        packageManager.getPackageArchiveInfo(
            packageName,
            PackageManager.PackageInfoFlags.of(flags.toLong())
        )
    } else {
        packageManager.getPackageArchiveInfo(packageName, flags)
    }

fun NotificationManager.getStatusBarNotification(vararg ids: Int): StatusBarNotification? {
    return activeNotifications.firstOrNull { ids.contains(it.id) }
}

fun NotificationManager.hasActiveNotification(vararg ids: Int): Boolean {
    return getStatusBarNotification(*ids) != null
}

fun Context.hasPermission(permission: String): Boolean {
    return ContextCompat.checkSelfPermission(
        this,
        permission
    ) == PackageManager.PERMISSION_GRANTED
}

inline fun <reified T : Parcelable> Intent.parcelable(key: String): T? = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU -> getParcelableExtra(
        key,
        T::class.java
    )

    else -> @Suppress("DEPRECATION") getParcelableExtra(key) as? T
}

