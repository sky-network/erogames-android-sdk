package com.erogames.updater.entity

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class JwtTokenResp(val token: String)