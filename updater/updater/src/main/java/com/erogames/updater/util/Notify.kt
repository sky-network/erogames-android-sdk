package com.erogames.updater.util

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.erogames.updater.R

internal object Notify {
    const val DOWNLOAD_NOTIFICATION_ID = 101
    const val DONE_NOTIFICATION_ID = 102
    const val DOWNLOAD_CHANNEL_ID = "ceu_notification_channel_id"
    const val DONE_CHANNEL_ID = "ceu_done_channel_id"

    fun createNotificationChannels(ctx: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val downloadIChannel =
                NotificationChannelCompat.Builder(
                    DOWNLOAD_CHANNEL_ID,
                    NotificationManagerCompat.IMPORTANCE_MIN
                )
                    .setName(ctx.getString(R.string.com_erogames_updater_ntfcn_channel_download))
                    .setSound(null, null)
                    .build()

            val doneChannel =
                NotificationChannelCompat.Builder(
                    DONE_CHANNEL_ID,
                    NotificationManagerCompat.IMPORTANCE_MAX
                )
                    .setName(ctx.getString(R.string.com_erogames_updater_ntfcn_channel_download_done))
                    .build()

            NotificationManagerCompat.from(ctx).apply {
                createNotificationChannel(downloadIChannel)
                createNotificationChannel(doneChannel)
            }
        }
    }

    fun getDownloadNotification(
        ctx: Context,
        chanelId: String
    ): Notification {
        val title = ctx.getString(R.string.com_erogames_updater_ntfcn_title)

        return NotificationCompat.Builder(ctx, chanelId)
            .setContentTitle(title)
            .setSound(null)
            .setOngoing(true)
            .setAutoCancel(true)
            .setSmallIcon(R.drawable.com_erogames_updater_ntfcn_small_icon)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setPriority(NotificationManagerCompat.IMPORTANCE_MIN)
            .setCategory(NotificationCompat.CATEGORY_SERVICE)
            .build()
    }

    fun getDoneNotification(
        ctx: Context,
        chanelId: String,
        intent: PendingIntent? = null
    ): Notification {
        val title = ctx.getString(R.string.com_erogames_updater_ntfcn_title)
        val text = ctx.getString(R.string.com_erogames_updater_ntfcn_text)
        val actionText = ctx.getString(R.string.com_erogames_updater_ntfcn_action_button)

        return NotificationCompat.Builder(ctx, chanelId)
            .setSmallIcon(R.drawable.com_erogames_updater_ntfcn_small_icon)
            .setAutoCancel(true)
            .setContentTitle(title)
            .setContentText(text)
            .setContentIntent(intent)
            .addAction(0, actionText, intent)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setPriority(NotificationManagerCompat.IMPORTANCE_HIGH)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .build()
    }
}