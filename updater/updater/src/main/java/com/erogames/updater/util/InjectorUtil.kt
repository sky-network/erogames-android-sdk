package com.erogames.updater.util

import android.content.Context
import androidx.preference.PreferenceManager
import com.erogames.updater.api.ApiService
import com.erogames.updater.repository.Repository
import com.erogames.updater.repository.Storage
import kotlinx.serialization.json.Json

internal object InjectorUtil {
    private var apiService: ApiService? = null
    private var repo: Repository? = null
    private var storage: Storage? = null
    val myJson: Json by lazy { Json { ignoreUnknownKeys = true } }

    @JvmStatic
    private fun provideApiService() =
        apiService ?: ApiService.create().also { apiService = it }

    @JvmStatic
    fun provideRepository(ctx: Context): Repository {
        return repo ?: Repository(
            provideApiService(),
            provideStorage(ctx.applicationContext)
        ).also { repo = it }
    }

    @JvmStatic
    private fun provideStorage(context: Context) = storage ?: Storage(
        PreferenceManager.getDefaultSharedPreferences(context)
    ).also { storage = it }
}