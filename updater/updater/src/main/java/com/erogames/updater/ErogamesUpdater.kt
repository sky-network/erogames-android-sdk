package com.erogames.updater

import android.content.Context
import androidx.annotation.Keep
import com.erogames.updater.service.UpdateListener
import com.erogames.updater.service.UpdateService
import com.erogames.updater.util.InjectorUtil
import com.erogames.updater.util.Notify
import java.util.Locale

@Keep
object ErogamesUpdater {

    @JvmStatic
    fun init(ctx: Context, accessKey: String, whitelabelId: String, locale: Locale) {
        Notify.createNotificationChannels(ctx)
        InjectorUtil.provideRepository(ctx).apply {
            setWhitelabelId(whitelabelId)
            setAccessKey(accessKey)
            setLocale(locale)
        }
    }

    @JvmStatic
    fun update(ctx: Context, updateListener: UpdateListener? = null) {
        UpdateService.update(ctx, updateListener)
    }

    @JvmStatic
    fun stop(ctx: Context) {
        UpdateService.stop(ctx)
    }
}