package com.erogames.updater.service

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import androidx.annotation.Keep
import androidx.annotation.WorkerThread
import androidx.core.app.NotificationCompat
import androidx.core.app.ServiceCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.erogames.patcher.ErogamesPatcher
import com.erogames.updater.entity.GameItem
import com.erogames.updater.entity.Patch
import com.erogames.updater.repository.Repository
import com.erogames.updater.util.InjectorUtil
import com.erogames.updater.util.Notify
import com.erogames.updater.util.getPackageArchiveInfoCompat
import com.erogames.updater.util.getPackageInfoCompat
import com.erogames.updater.util.hasActiveNotification
import com.erogames.updater.util.hasPermission
import com.erogames.updater.util.parcelable
import com.erogames.updater.util.versionName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import net.swiftzer.semver.SemVer
import java.io.File


internal class UpdateService : LifecycleService(), LifecycleEventObserver {

    private var isStopping: Boolean = false
    private var isAppInForeground = false
    private var updateState: UpdateState = UpdateState()
    private var resultReceiver: ResultReceiver? = null
    private val repository: Repository by lazy {
        InjectorUtil.provideRepository(this)
    }
    private val notificationManager: NotificationManager by lazy {
        getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    }


    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        notificationManager.cancelAll()
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_RESUME -> isAppInForeground = true
            Lifecycle.Event.ON_PAUSE -> isAppInForeground = false
            else -> Unit
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Log.d(TAG, "Action: ${intent?.action}")
        startForegroundFallback()

        if (isStopping) {
            Log.d(TAG, "Stopping... Ignore it.")
            return START_NOT_STICKY
        }

        if (intent?.action == ACTION_UPDATE && updateState.state == UpdateState.State.RUNNING) {
            Log.d(TAG, "Already running. Ignore it.")
            return START_STICKY
        }

        when (intent?.action) {
            ACTION_UPDATE -> onUpdateAction(intent)
            ACTION_STOP -> onStopAction()
        }

        return START_STICKY
    }

    private val callServiceType: Int
        @SuppressLint("InlinedApi")
        get() {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
                ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC
            else 0
        }


    private fun startForegroundFallback() {
        val hasNotification =
            notificationManager.hasActiveNotification(Notify.DOWNLOAD_NOTIFICATION_ID)
        if (!hasNotification) {
            val notification = Notify.getDownloadNotification(
                this,
                Notify.DOWNLOAD_CHANNEL_ID
            )

            ServiceCompat.startForeground(
                this,
                Notify.DOWNLOAD_NOTIFICATION_ID,
                notification,
                callServiceType
            )
        }
    }

    private fun hasUpdate(gameItem: GameItem): Boolean {
        val localVersion: SemVer = SemVer.parse(versionName())
        val serverVersion: SemVer = SemVer.parse(gameItem.version ?: "")
        Log.i(TAG, "Game item ($packageName) local version: $localVersion")
        Log.i(TAG, "Game item ($packageName) server version: $serverVersion")

        return serverVersion.compareTo(localVersion) == 1
    }

    private fun onUpdateAction(intent: Intent) {
        resultReceiver = intent.parcelable(EXTRA_RESULT_RECEIVER) as ResultReceiver?
        setUpdateState(UpdateState(UpdateState.State.RUNNING, 0, null))
        lifecycleScope.launch(Dispatchers.IO) {
            Log.d(TAG, "Load game item...")
            val gameItemResp = repository.getGameItem(packageName)
            if (gameItemResp.isFailure) {
                val err = gameItemResp.exceptionOrNull()?.message ?: UNKNOWN_ERROR
                setUpdateState(UpdateState(UpdateState.State.FAILURE, 0, err))
                stop(this@UpdateService)
                return@launch
            }

            val gameItem = gameItemResp.getOrNull()!!
            if (!hasUpdate(gameItem)) {
                Log.d(TAG, "There is no update for game item ($packageName).")
                setUpdateState(UpdateState(UpdateState.State.DONE, 0, null))
                stop(this@UpdateService)
                return@launch
            }

            val whitelabelId = repository.getWhitelabelId()
            val filename = "${whitelabelId}_${gameItem.slug}@${gameItem.version}.apk"
            val apkFile = File(getDownloadsDir(), filename)
            val archiveInfo = getPackageArchiveInfoCompat(apkFile.absolutePath, 0)

            if (archiveInfo != null) {
                if (archiveInfo.packageName != packageName) {
                    val err = "Invalid archive (packageName: ${archiveInfo.packageName})"
                    setUpdateState(UpdateState(UpdateState.State.FAILURE, 0, err))
                    stop(this@UpdateService)
                    return@launch
                }

                Log.i(TAG, "Already downloaded. Trying to open...")
                setUpdateState(UpdateState(UpdateState.State.DONE, 100, null))
                openFile(apkFile.absolutePath)
            } else {
                val downloadResp = download(gameItem, gameItem.getPatch(versionName()))
                if (downloadResp.isSuccess) {
                    setUpdateState(UpdateState(UpdateState.State.DONE, 100, null))
                    openFile(apkFile.absolutePath)
                } else {
                    val err = gameItemResp.exceptionOrNull()?.message ?: UNKNOWN_ERROR
                    setUpdateState(UpdateState(UpdateState.State.FAILURE, 100, err))
                }
            }
            stop(this@UpdateService)
        }
    }

    private fun setUpdateState(updateState: UpdateState) {
        this.updateState = updateState
        resultReceiver?.send(0, UpdateListener.toBundle(updateState))
    }

    private fun openFile(filePath: String) {
        val data = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            FileProvider.getUriForFile(this, "$packageName.provider", File(filePath))
        } else {
            Uri.fromFile(File(filePath))
        }
        val intent = Intent().apply {
            action = Intent.ACTION_INSTALL_PACKAGE
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_ACTIVITY_NEW_TASK
            setDataAndType(data, "application/vnd.android.package-archive")
        }

        if (isAppInForeground) startActivity(intent)
        else showDoneNotification(intent)
    }

    private fun showDoneNotification(intent: Intent) {
        val flag =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) PendingIntent.FLAG_IMMUTABLE
            else PendingIntent.FLAG_UPDATE_CURRENT
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, flag)

        notificationManager.notify(
            Notify.DONE_NOTIFICATION_ID,
            Notify.getDoneNotification(this, Notify.DONE_CHANNEL_ID, pendingIntent)
        )
    }

    private suspend fun download(gameItem: GameItem, patch: Patch?): Result<File> {
        val patchOrApk = if (patch != null) "patch" else "full APK"
        Log.i(TAG, "Download $patchOrApk...")

        return runCatching {
            val whitelabelId = repository.getWhitelabelId()
            val ext = if (patch != null) "path" else "apk"
            val outFileName = "${whitelabelId}_${gameItem.slug}@${gameItem.version}.$ext"
            val outApkFileName = "${whitelabelId}_${gameItem.slug}@${gameItem.version}.apk"
            val outFile = File(getDownloadsDir(), outFileName)
            val outApkFile = File(getDownloadsDir(), outApkFileName)
            val downloadUrl = (patch?.downloadUrl ?: gameItem.downloadUrl)!!
            val packageInfo = getPackageInfoCompat(packageName, 0)
            val oldFile = File(packageInfo.applicationInfo.sourceDir)

            if (outFile.exists()) outFile.delete()
            if (outApkFile.exists()) outApkFile.delete()

            repository.download(downloadUrl, outFile) { progress ->
                setUpdateState(UpdateState(UpdateState.State.RUNNING, progress, null))
                updateNotificationProgress(progress)
            }.getOrThrow()

            if (patch != null) {
                Log.i(TAG, "Applying patch...")
                if (!patch(oldFile, outFile, outApkFile)) {
                    Log.w(TAG, "Applying patch failed. Download full apk...")
                    download(gameItem, null).getOrThrow()
                } else {
                    outApkFile
                }
            } else {
                outFile
            }
        }
    }

    private fun updateNotificationProgress(progress: Int) {
        notificationManager.notify(
            Notify.DOWNLOAD_NOTIFICATION_ID,
            NotificationCompat.Builder(
                this,
                Notify.getDownloadNotification(this, Notify.DOWNLOAD_CHANNEL_ID),
            )
                .setProgress(100, progress, false)
                .build()
        )
    }

    @WorkerThread
    private suspend fun patch(oldFile: File, patchFile: File, patchedFile: File): Boolean {
        withContext(Dispatchers.IO) {
            try {
                val result = ErogamesPatcher.patch(oldFile, patchFile, patchedFile)
                return@withContext result == 0
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return false
    }

    private fun getDownloadsDir(): File {
        return getExternalFilesDir(null) ?: filesDir
    }

    private fun onStopAction() {
        isStopping = true
        cleanService()
        stopSelf()
    }

    private fun cleanService() {}

    override fun onDestroy() {
        ProcessLifecycleOwner.get().lifecycle.removeObserver(this)
        cleanService()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "UpdateService"
        private const val ACTION_UPDATE = "com.erogames.updater.ACTION_UPDATE"
        private const val ACTION_STOP = "com.erogames.updater.ACTION_STOP"
        internal const val EXTRA_RESULT = "com.erogames.updater.EXTRA_RESULT"
        private const val EXTRA_RESULT_RECEIVER = "com.erogames.updater.EXTRA_RESULT_RECEIVER"

        private const val UNKNOWN_ERROR = "Unknown error"

        internal fun update(ctx: Context, resultReceiver: UpdateListener?) {
            if (!hasPermissions(ctx)) return

            val i = Intent(ctx.applicationContext, UpdateService::class.java).apply {
                action = ACTION_UPDATE
                putExtra(EXTRA_RESULT_RECEIVER, object : ResultReceiver(null) {
                    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
                        super.onReceiveResult(resultCode, resultData)
                        resultReceiver?.onResult(UpdateListener.fromBundle(resultData))
                    }
                })
            }
            ContextCompat.startForegroundService(ctx.applicationContext, i)
        }

        internal fun stop(ctx: Context) {
            if (!hasPermissions(ctx)) return

            val i = Intent(ctx.applicationContext, UpdateService::class.java).apply {
                action = ACTION_STOP
            }
            ContextCompat.startForegroundService(ctx.applicationContext, i)
        }

        private fun hasPermissions(ctx: Context): Boolean {
            val atLeastTiramisu = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU
            val hasPermission = when {
                atLeastTiramisu -> ctx.hasPermission(Manifest.permission.POST_NOTIFICATIONS)
                else -> true
            }
            if (!hasPermission) {
                Log.w(TAG, "Permission 'POST_NOTIFICATIONS' is denied.")
            }
            return hasPermission
        }
    }
}

@Keep
interface UpdateListener {
    fun onResult(state: UpdateState?)

    companion object {
        fun fromBundle(resultData: Bundle?): UpdateState? {
            val stateStr = resultData?.getString(UpdateService.EXTRA_RESULT)
            if (stateStr.isNullOrEmpty()) return null
            return InjectorUtil.myJson.decodeFromString<UpdateState>(stateStr)
        }

        fun toBundle(data: UpdateState): Bundle {
            val stateStr = InjectorUtil.myJson.encodeToString(data)
            return bundleOf(UpdateService.EXTRA_RESULT to stateStr)
        }
    }
}

@Keep
@Serializable
data class UpdateState(
    val state: State = State.IDLE,
    val progress: Int = 0,
    val message: String? = null
) {
    enum class State { IDLE, RUNNING, DONE, FAILURE }
}
