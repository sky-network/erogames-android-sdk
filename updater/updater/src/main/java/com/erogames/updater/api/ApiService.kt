package com.erogames.updater.api

import com.erogames.updater.BuildConfig
import com.erogames.updater.entity.GameItemsResp
import com.erogames.updater.entity.JwtTokenResp
import com.erogames.updater.entity.WhiteLabelsResp
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.onDownload
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.prepareGet
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import io.ktor.util.flattenEntries
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.core.isEmpty
import io.ktor.utils.io.core.readBytes
import kotlinx.serialization.json.Json
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import kotlin.math.max

internal class ApiService(private val ktor: HttpClient) {

    suspend fun getJwtToken(accessKey: String): Result<JwtTokenResp> =
        runCatching {
            ktor.post("/api/v1/authenticate") {
                setBody(mapOf("access_key" to accessKey))
            }.body()
        }

    suspend fun getWhitelabels(jwtToken: String): Result<WhiteLabelsResp> =
        runCatching {
            ktor.get("/api/v1/whitelabels") {
                header("Authorization", jwtToken)
            }.body()
        }

    suspend fun getGameItems(
        baseUrl: String,
        jwtToken: String,
        params: Map<String, String> = mapOf()
    ): Result<GameItemsResp> =
        runCatching {
            val ktor2 = ktor
            val paramsCopy = params.toMutableMap()
            var response: HttpResponse? = null
            var location: String? = "${baseUrl}/api/v1/items"
            while (!location.isNullOrEmpty()) {
                response = ktor2.config {
                    followRedirects = false
                    expectSuccess = false
                }.get(location) {
                    header("Authorization", jwtToken)
                    paramsCopy.entries.map { parameter(it.key, it.value) }
                }
                location = response.headers.flattenEntries()
                    .firstOrNull { (name, _) -> name.equals("location", true) }?.second
                paramsCopy.clear()
            }
            response!!.body()
        }

    suspend fun download(
        url: String,
        outputFile: File,
        onDownload: ((progress: Int) -> Unit)? = null
    ) = runCatching {
        var progress = 0
        ktor.prepareGet(url) {
            contentType(ContentType.Application.OctetStream)
            onDownload { part, total ->
                val newProgress: Int = (part * 100L / max(1, total)).toInt()
                if ((newProgress % 5 == 0) && newProgress != progress) {
                    progress = newProgress
                    onDownload?.invoke(progress)
                }
            }
        }.execute { httpResponse ->
            val channel: ByteReadChannel = httpResponse.body()
            while (!channel.isClosedForRead) {
                val packet = channel.readRemaining(DEFAULT_BUFFER_SIZE.toLong())
                while (!packet.isEmpty) {
                    val bytes = packet.readBytes()
                    outputFile.appendBytes(bytes)
                }
            }
        }
    }

    companion object {

        private const val BASE_URL = "https://erogames.com"

        @JvmStatic
        fun create(): ApiService {
            val ktor = HttpClient(OkHttp) {
                expectSuccess = true
                engine {
//                    if (BuildConfig.DEBUG) {
//                        addNetworkInterceptor(HttpLoggingInterceptor().apply {
//                            level = HttpLoggingInterceptor.Level.BODY
//                        })
//                    }
                }
                defaultRequest {
                    contentType(ContentType.Application.Json.withParameter("charset", "utf-8"))
                    header("X-SDK-VERSION", "1.1")
                    url(BASE_URL)
                }
                install(ContentNegotiation) {
                    json(Json {
                        ignoreUnknownKeys = true
                        encodeDefaults = true
                        isLenient = true
                        allowSpecialFloatingPointValues = true
                        allowStructuredMapKeys = true
                        prettyPrint = false
                        useArrayPolymorphism = false
                    })
                }
            }
            return ApiService(ktor)
        }
    }
}