package com.erogames.updater.entity

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class GameItemsResp(val items: List<GameItem>)