package com.erogames.updater.repository

import com.erogames.updater.api.ApiService
import com.erogames.updater.entity.GameItem
import java.io.File
import java.util.Locale

internal class Repository(
    private val apiService: ApiService,
    private val storage: Storage
) {

    suspend fun getGameItem(appId: String): Result<GameItem> = runCatching {
        val accessKey = getAccessKey()
        val whitelabelId = getWhitelabelId()
        val language = getLocale().language
        val params = mapOf("locale" to language, "platform" to "android")

        val jwtToken = apiService.getJwtToken(accessKey).getOrThrow().token
        val whitelabel = getWhitelabel(jwtToken, whitelabelId).getOrThrow()
        getGameItemById(whitelabel.url, jwtToken, appId, params).getOrThrow()
    }

    private suspend fun getGameItemById(
        baseUrl: String,
        jwtToken: String,
        appId: String,
        params: Map<String, String> = mapOf()
    ) = runCatching {
        val gameItem = apiService.getGameItems(baseUrl, jwtToken, params)
            .getOrThrow().items
            .firstOrNull { it.appId == appId }
            ?: throw IllegalArgumentException("Game id: $appId")
        val patches = gameItem.patches.map { it.copy(newVersion = gameItem.version) }
        gameItem.copy(patches = patches)
    }

    private suspend fun getWhitelabel(jwtToken: String, wlId: String) =
        runCatching {
            apiService.getWhitelabels(jwtToken).getOrThrow().whitelabels
                .firstOrNull { it.slug == wlId }
                ?: throw IllegalArgumentException("Invalid whitelabel id: $wlId")
        }

    suspend fun download(
        url: String,
        outputFile: File,
        onDownload: ((progress: Int) -> Unit)? = null
    ) = apiService.download(url, outputFile, onDownload)

    fun setWhitelabelId(whitelabelId: String) {
        storage.setWhitelabelId(whitelabelId)
    }

    fun getWhitelabelId(): String {
        return storage.getWhitelabelId()!!
    }

    fun setAccessKey(accessKey: String) {
        storage.setAccessKey(accessKey)
    }

    private fun getAccessKey(): String {
        return storage.getAccessKey()!!
    }

    fun setLocale(locale: Locale) {
        storage.setLocale(locale)
    }

    private fun getLocale(): Locale {
        return storage.getLocale()!!
    }
}