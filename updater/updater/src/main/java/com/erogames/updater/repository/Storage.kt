package com.erogames.updater.repository

import android.content.SharedPreferences
import java.util.Locale

internal class Storage(private val prefs: SharedPreferences) {

    fun setWhitelabelId(whitelabelId: String) =
        prefs.edit().putString(PREF_WHITELABEL_ID, whitelabelId).apply()

    fun getWhitelabelId(): String? = prefs.getString(PREF_WHITELABEL_ID, null)

    fun setAccessKey(accessKey: String) =
        prefs.edit().putString(PREF_ACCESS_KEY, accessKey).apply()

    fun getAccessKey(): String? = prefs.getString(PREF_ACCESS_KEY, null)

    fun setLocale(locale: Locale) =
        prefs.edit().putString(PREF_LOCALE, locale.language).apply()

    fun getLocale(): Locale? {
        val language = prefs.getString(PREF_LOCALE, null)
        if (language.isNullOrEmpty()) return null
        return Locale(language)
    }

    companion object {
        private const val PREF_WHITELABEL_ID = "com_erogames_updater_pref_whitelabel_id"
        private const val PREF_ACCESS_KEY = "com_erogames_updater_pref_access_key"
        private const val PREF_LOCALE = "com_erogames_updater_pref_locale"
    }
}