package com.erogames.updater.entity

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class GameItem(
    val slug: String? = null,
    @SerialName("app_id")
    val appId: String? = null,
    @SerialName("rawVersionCode")
    val version: String? = null,
    @SerialName("download_url")
    val fileUrl: String? = null,
    @SerialName("apk_file_url")
    val apkFileUrl: String? = null,
    val patches: List<Patch> = listOf()
) {
    val downloadUrl get() = fileUrl ?: apkFileUrl

    fun getPatch(oldVersion: String): Patch? {
        return patches.firstOrNull { it.oldVersion == oldVersion }
    }
}

@Keep
@Serializable
data class Patch(
    @SerialName("old_version")
    val oldVersion: String? = null,
    val newVersion: String? = null,
    @SerialName("url")
    val downloadUrl: String? = null,
    @SerialName("dir_diff")
    val dirDiff: Boolean? = null
)
