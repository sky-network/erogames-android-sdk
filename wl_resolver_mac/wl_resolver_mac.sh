#!/bin/bash
#
# version: 1.0.0
#

readonly COLOR_RED=$(tput setaf 1)
readonly COLOR_GREEN=$(tput setaf 2)
readonly STYLE_RESET=$(tput sgr0)
TMP_DIR=$(mktemp -d "${TMPDIR:-/tmp}"/sky_mac_wl.XXXXXXXX)
declare -r TMP_DIR

# Init named arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
  --whitelabel)
    declare -r WHITELABEL="$2"
    shift
    ;;
  --entitlements)
    declare -r ENTITLEMENTS="$2"
    shift
    ;;
  --sign)
    declare -r SIGN="$2"
    shift
    ;;
  --keychain-profile)
    declare -r KEYCHAIN_PROFILE="$2"
    shift
    ;;
  --in)
    declare -r INPUT="$2"
    shift
    ;;
  --out)
    declare OUTPUT="$2"
    shift
    ;;
  *) ;;
  esac
  shift
done

#######################################
# Сolored 'echo'
# Example 1:
#   echolog "some_string"
#   out: {prefix} some_string
# Example 2:
#   echolog "some_string" ""
#   out: some_string
# Example 3:
#   echolog "some_string" "$COLOR_RED"
#   out: {RED prefix} some_string
# Globals:
#   COLOR_RED, COLOR_GREEN, STYLE_RESET
# Arguments:
#    String to output.
#    (Option) Prefix color. Prefix will not be printed if this arg is empty.
#######################################
function echolog() {
  if [[ "$#" -gt 1 ]]; then
    if [ -z "${2}" ]; then
      prefix=""
      prefix_color="$STYLE_RESET"
    else
      prefix="==>"
      prefix_color="${2}"
    fi
  else
    prefix="==>"
    prefix_color="$COLOR_GREEN"
  fi
  echo "${prefix_color}${prefix}${STYLE_RESET} ${1}"
}

#######################################
# Cleanup dir if exists.
# Arguments:
#    Dir path to cleanup.
#######################################
function remove_dir() {
  local dir_to_remove="${1}"
  if [ -d "$dir_to_remove" ]; then
    echolog "Remove dir: $dir_to_remove" "$COLOR_GREEN"
    rm -rf "$dir_to_remove"
  fi
}

#######################################
# Remove file if exists.
# Arguments:
#    File path to remove.
#######################################
function remove_file() {
  local file_remove="${1}"
  if [ -e "$file_remove" ]; then
    echolog "Remove: $file_remove" "$COLOR_GREEN"
    rm -f "$OUTPUT"
  fi
}

function cleanup_and_exit() {
  remove_dir "$TMP_DIR"
  remove_file "$OUTPUT"
  exit
}

function check_signature() {
  local signed_file="${1}"
  codesign -dv --verbose=4 "$signed_file"
}

function sign() {
  local in_file="${1}"
  local sign_params=(--deep --force --timestamp --options runtime)
  if [ -n "$ENTITLEMENTS" ]; then
    sign_params+=(--entitlements "$ENTITLEMENTS")
  fi

  sign_params+=(--sign "$SIGN" "$in_file")
  codesign "${sign_params[@]}"
}

if [ -z "$WHITELABEL" ]; then
  echolog "Please specify the '--whitelabel'." "$COLOR_RED"
  exit 1
fi

if [ -z "$SIGN" ]; then
  echolog "Please specify the '--sign'." "$COLOR_RED"
  exit 1
fi

if [ -z "$KEYCHAIN_PROFILE" ]; then
  echolog "Please specify the '--keychain-profile'." "$COLOR_RED"
  exit 1
fi

if [ -z "$INPUT" ]; then
  echolog "Please specify the '--in'." "$COLOR_RED"
  exit 1
fi

if [ ! -f "$INPUT" ]; then
  echolog "File not found: $INPUT" "$COLOR_RED"
  exit 1
fi

if [ -z "$OUTPUT" ]; then
  echolog "Please specify the '--out'." "$COLOR_RED"
  exit 1
fi

##############################
# 1. Create temp dir if not exists.
if [ -d "$TMP_DIR" ]; then
  echolog "Clean up temp dir..." "$COLOR_GREEN"
  remove_dir "$TMP_DIR"
fi

output_file_name=$(basename "$OUTPUT")
declare -r output_file_name
declare OUTPUT_TMP="$TMP_DIR/${output_file_name}.tmp.zip"
declare -r OUTPUT_TMP

# Unzip archive into the temp dir.
echolog "Unzip archive..." "$COLOR_GREEN"
unzip -q -d "$TMP_DIR" "$INPUT" || cleanup_and_exit
echolog "Unzipped to: $TMP_DIR" "$COLOR_GREEN"

# Set .app file path
APP_PATH=$(find "$TMP_DIR" -type d -name "*.app" | head -n 1)
declare -r APP_PATH
if [ -z "$APP_PATH" ]; then
  echolog "File not found: $APP_PATH" "$COLOR_RED"
  cleanup_and_exit
fi

# Add erogames_whitelabel_id file into "*.app/Contents" folder.
echolog "Set whitelabel: $WHITELABEL" "$COLOR_GREEN"
: >"$APP_PATH/Contents/erogames_whitelabel_id"
printf %s "$WHITELABEL" >>"$APP_PATH/Contents/erogames_whitelabel_id"

# Sign .app
echolog "Sign $APP_PATH" "$COLOR_GREEN"
sign "$APP_PATH" || cleanup_and_exit
check_signature "$APP_PATH" || cleanup_and_exit

# Compress
echolog "Compress..." "$COLOR_GREEN"
ditto -c -k --sequesterRsrc --keepParent "$APP_PATH" "$OUTPUT_TMP" || cleanup_and_exit

# Notary
echolog "Notary..." "$COLOR_GREEN"
xcrun notarytool submit "$OUTPUT_TMP" --keychain-profile "$KEYCHAIN_PROFILE" --wait || cleanup_and_exit

# Staple
echolog "Staple..." "$COLOR_GREEN"
xcrun stapler staple "$APP_PATH"

# Remove zip
rm "$OUTPUT_TMP"

# Compress again
echolog "Compress again..." "$COLOR_GREEN"
ditto -c -k --sequesterRsrc --keepParent "$APP_PATH" "$OUTPUT_TMP"
rm "$OUTPUT"
echolog "Move to $OUTPUT" "$COLOR_GREEN"
mv "$OUTPUT_TMP" "$OUTPUT"

# 4. Remove temp dir
remove_dir "$TMP_DIR"
echolog "Done." "$COLOR_GREEN"
exit
