# Whitelabel Resolver Script (macOS)

...

### How to use
```
sh wl_resolver_mac.sh --whitelabel <whitelabel> \
  --entitlements <path_to.entitlements> \
  --sign "Developer ID Application: <your_id>" \
  --keychain-profile "<your_profile>"
  --in "<path_to_unity_macos_game.zip>" \
  --out "<path_to_unity_macos_game_out.zip>"
```
