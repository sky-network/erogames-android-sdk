package com.erogames.api.util

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle

internal class Utils private constructor() {

    companion object {
        //private const val TAG = "Utils"

        fun getAppMetadata(ctx: Context): Bundle? {
            val applicationInfo: ApplicationInfo = ctx.packageManager.getApplicationInfo(
                ctx.packageName,
                PackageManager.GET_META_DATA
            )
            return applicationInfo.metaData
        }
    }
}