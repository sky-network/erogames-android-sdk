package com.erogames.api.api

import android.content.Context
import com.erogames.api.BuildConfig
import com.erogames.api.R
import com.erogames.api.model.*
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.http.*

internal interface ApiService {

    @FormUrlEncoded
    @POST("api/v1/authenticate")
    suspend fun getJwtToken(
        @Field("access_key") accessKey: String?,
    ): JwtTokenResp

    @GET("api/v1/whitelabels")
    suspend fun getWhitelabels(@Header("Authorization") jwtToken: String): WhiteLabelsResp

    @GET
    suspend fun getUser(
        @Url profileUrl: String,
        @Header("Authorization") accessToken: String,
    ): UserResp

    @FormUrlEncoded
    @POST
    suspend fun proceedPayment(
        @Url paymentUrl: String,
        @Header("Authorization") accessToken: String,
        @Field("paymentId") paymentId: String?,
        @Field("amount") amount: Int?,
    ): BaseStatusResp

    @PUT
    suspend fun addDataPoints(
        @Url dataPointsUrl: String,
        @Header("Authorization") accessToken: String,
        @Body dataPoints: DataPointModel,
    ): BaseStatusResp

    @GET
    suspend fun getCurrentQuestData(
        @Url url: String,
        @Header("Authorization") accessToken: String,
    ): QuestData

    @GET
    suspend fun getPaymentInfo(
        @Url url: String,
        @Header("Authorization") accessToken: String,
    ): PaymentInfo

    companion object {

        @Volatile
        private var instance: ApiService? = null
        private const val X_SDK_VERSION: String = "1.1"

        internal const val USER_ENDPOINT = "/api/v1/me"
        internal const val QUEST_ENDPOINT = "/api/v1/quests/current"
        internal const val PAYMENTS_ENDPOINT = "/api/v1/payments"
        internal const val DATA_POINTS_ENDPOINT = "/api/v1/me/data_point_collection"
        internal const val PAYMENT_INFO_ENDPOINT = "/api/v1/payments/%s"

        @ExperimentalSerializationApi
        @JvmStatic
        fun getInstance(ctx: Context): ApiService {
            return instance ?: synchronized(this) {
                if (instance == null) {
                    val clientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
                    clientBuilder.addInterceptor(ErrorInterceptor())
                    clientBuilder.addInterceptor(Interceptor { chain ->
                        val request: Request = chain.request().newBuilder()
                            .addHeader("X-SDK-VERSION", X_SDK_VERSION)
                            .build()
                        chain.proceed(request)
                    })

                    if (BuildConfig.DEBUG) {
                        val logging = HttpLoggingInterceptor()
                        logging.level = HttpLoggingInterceptor.Level.BODY
                        clientBuilder.addNetworkInterceptor(logging)
                    }

                    val contentType = "application/json".toMediaType()
                    val retrofitBuilder = Retrofit.Builder()
                        .baseUrl(ctx.getString(R.string.com_erogames_api_base_url))
                        .addConverterFactory(Json {
                            ignoreUnknownKeys = true
                        }.asConverterFactory(contentType))
                    retrofitBuilder.client(clientBuilder.build())

                    return retrofitBuilder.build()
                        .create(ApiService::class.java).also { instance = it }
                }
                return instance!!
            }
        }
    }
}